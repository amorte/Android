package dts.com.br.exemplobroadcastnotificacao;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import dts.com.br.exemplobroadcastnotificacao.Util.ConnetionChanged;

public class MainActivity extends AppCompatActivity implements ConnetionChanged{

    private TextView mTextView;

    private ConnectionReceiver mConnectionReveiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mTextView = new TextView(this);
        mTextView.setText((Util.getInstance(this).isOnline()) ? "Online" : "Offline");
        mTextView.setTextSize(100);

        Util.getInstance(this).registerListener(this);


        Util.getInstance(this).cancelNotification();

        setContentView(mTextView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        mConnectionReveiver = new ConnectionReceiver();
        registerReceiver(mConnectionReveiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mConnectionReveiver);
    }

    @Override
    public void onConnectionChanged() {
        mTextView.setText((Util.getInstance(this).isOnline()) ? "Online" : "Offline");
    }

    public class ConnectionReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            mTextView.setText((Util.getInstance(context).isOnline()) ? "Online" : "Offline");
        }
    }
}
