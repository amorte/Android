package br.com.dts.intents;

import android.Manifest;
import android.app.ListActivity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class MainActivity extends ListActivity {

    private final String options [] = {"Abir Browser", "Fazer ligação", "Msg. Whatsapp", "Abrir mapa em localicação", "Tirar foto", "Abrir galeria", "Outra App", "Video", "Abrir mapa endereço", "Sair" };
    private static final String sURL = "http://developer.android.com";
    private static final String sMESSAGE = "Olá, estou em aula";
    private static final String sPHONE = "99998888";
    private Intent mIntent;
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int REQUEST_VIDEO_CAPTURE = 2;
    private static final int REQUEST_CALL_PERMISSION = 1;
    private static final int REQUEST_CAMERA_PERMISSION = 2;
    private final String sVIDEO_PATH = Environment.getExternalStorageDirectory().getAbsolutePath()+"videocapture_example.mp4";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_selectable_list_item, options));

    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        switch (position){

            case 0:
                mIntent = new Intent(Intent.ACTION_VIEW);
                mIntent.setData(Uri.parse(sURL));
                startActivity(mIntent);
                break;

            case 1:
                int hasPhonePermission = checkSelfPermission(Manifest.permission.CALL_PHONE);
                if (hasPhonePermission != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{Manifest.permission.CALL_PHONE},
                            REQUEST_CALL_PERMISSION);
                } else {
                    //mIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + sPHONE));
                    startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + sPHONE)));
                }
                break;
            case 2:
                mIntent = new Intent(Intent.ACTION_SEND);
                mIntent.putExtra(Intent.EXTRA_TEXT, sMESSAGE);
                mIntent.setType("text/plain");
                mIntent.setPackage("com.whatsapp");
                try {
                    startActivity(mIntent);
                }catch (ActivityNotFoundException r){
                    //Qualquer ação
                    Toast.makeText(this, "Whatsapp não instalado", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + "com.whatsapp")));
                }

                break;
            case 3:
                //Uri mapUri = Uri.parse("geo:0,0?q=37.423156,-122.084917 (" + "Local" + ")");
                Uri mapUri = Uri.parse("geo:-8.052240,-34.871181?q=gas");

                mIntent = new Intent(Intent.ACTION_VIEW, mapUri);
                mIntent.setPackage("com.google.android.apps.maps");
                startActivity(mIntent);
                break;
            case 4:

                int hasCamera = checkSelfPermission(Manifest.permission.CAMERA);
                if (hasCamera != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{Manifest.permission.CAMERA},
                            REQUEST_CAMERA_PERMISSION);

                } else {
                    mIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

                    startActivityForResult(mIntent, REQUEST_IMAGE_CAPTURE);

                }
                break;

            case 5:
                mIntent = new Intent();
                mIntent.setAction(android.content.Intent.ACTION_VIEW);
                mIntent.setType("image/*");
                mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(mIntent);

                break;

            case 6:
                mIntent = new Intent();
                mIntent.setAction("br.com.dts.intentfilter.View");
                mIntent.addCategory("br.com.dts.intentfilter.filter");
                startActivity(mIntent);
                break;

            case 7:
                int hasVideoPermission = checkSelfPermission(Manifest.permission.CAMERA);
                if (hasVideoPermission != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{Manifest.permission.CAMERA},
                            REQUEST_CAMERA_PERMISSION);

                } else {
                    mIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                   // mIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 3000);
                    //mIntent.putExtra(MediaStore.EXTRA_OUTPUT, sVIDEO_PATH);

                    startActivityForResult(mIntent, REQUEST_VIDEO_CAPTURE);

                }
                break;

            case 8:
                startActivity(new Intent(this, MapAddressActivity.class));
                break;


            default:
                finish();
                break;
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Intent intent = new Intent(this, PictureActivity.class);
            intent.putExtras(data.getExtras());
            startActivity(intent);
        } else if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK){

            Intent intent = new Intent(this, VideoActivity.class);
            Uri videoUri = data.getData();

            intent.putExtra("EXTRA_VIDEO_PATH", videoUri.toString());
            startActivity(intent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CALL_PERMISSION:

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + sPHONE));
                    startActivity(mIntent);

                } else {
                    Toast.makeText(MainActivity.this, "Permission denied to phone app", Toast.LENGTH_SHORT).show();
                }
                break;


            case REQUEST_CAMERA_PERMISSION:

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    startActivityForResult(mIntent, REQUEST_IMAGE_CAPTURE);

                } else {
                    Toast.makeText(MainActivity.this, "Permission denied to read camera ", Toast.LENGTH_SHORT).show();
                }
                break;



        }
    }
}
