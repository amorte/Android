
package br.com.dts.knotintegration.util;

import android.content.Context;
import android.content.SharedPreferences;

import br.com.dts.knotintegration.application.App;

/**
 * The type Preference manager.
 */
public class PreferenceUtil {

    private static final String PREFERENCES_NAME = "knot_preference";
    private static final String KEY_END_POINT = "KNOT_URL";
    private static final String KEY_UUID = "USER_UUID";
    private static final String KEY_TOKEN = "USER_TOKEN";
    private static Object lock = new Object();

    private static PreferenceUtil sInstance;

    private PreferenceUtil() {
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static PreferenceUtil getInstance() {
        synchronized (lock) {
            if (sInstance == null) {
                sInstance = new PreferenceUtil();
            }
            return sInstance;
        }

        
    }

    /**
     * Reset all sharedPreferences
     */
    public void reset() {
        getPref().edit().clear().apply();
    }

    private SharedPreferences getPref() {
        final Context context = App.getContext();
        return context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    /**
     * Gets cloud end point.
     *
     * @return the cloud end point value
     */
    public String getEndPoint() {
        return getPref().getString(KEY_END_POINT, Util.EMPTY_STRING);
    }

    /**
     * Sets cloud end point.
     *
     * @param endPoint the cloud end point.
     */
    public void setEndPoint(String endPoint) {
        getPref().edit().putString(KEY_END_POINT, endPoint).apply();
    }

    /**
     * Gets owner UUID.
     *
     * @return the owner UUID
     */
    public String getUuid() {
        return getPref().getString(KEY_UUID, Util.EMPTY_STRING);
    }

    /**
     * Sets owner UUID.
     *
     * @param uuid the owner UUID.
     */
    public void setUuid(String uuid) {
        getPref().edit().putString(KEY_UUID, uuid).apply();
    }

    /**
     * Gets owner token.
     *
     * @return the owner token
     */
    public String getTonken() {
        return getPref().getString(KEY_TOKEN, Util.EMPTY_STRING);
    }

    /**
     * Sets owner token.
     *
     * @param token the owner token.
     */
    public void setToken(String token) {
        getPref().edit().putString(KEY_TOKEN, token).apply();
    }


}
