
package br.com.dts.knotintegration.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import br.com.dts.knotintegration.R;
import br.com.dts.knotintegration.util.PreferenceUtil;
import br.com.dts.knotintegration.util.Util;


public class SettingsActivity extends Activity {

    private EditText mEdtEndPoint;
    private EditText mEdtUUID;
    private EditText mEdtToken;
    private Button mBtnSave;
    private CoordinatorLayout mRootLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        initView();
    }

    private void initView() {
        mEdtEndPoint = (EditText) findViewById(R.id.edt_end_point);
        mEdtUUID = (EditText) findViewById(R.id.edt_uuid_owner);
        mEdtToken = (EditText) findViewById(R.id.edt_token_owner);
        mBtnSave = (Button) findViewById(R.id.btn_save);
        mRootLayout = (CoordinatorLayout) findViewById(R.id.rootCoordinatorLayout);


        mEdtEndPoint.setText(PreferenceUtil.getInstance().getEndPoint());
        mEdtUUID.setText(PreferenceUtil.getInstance().getUuid());
        mEdtToken.setText(PreferenceUtil.getInstance().getTonken());


        mEdtEndPoint.setText("http://knot-test.cesar.org.br:3000");
        mEdtToken.setText("8449949da1e33c0949e53210bab6696eb9d9a4d3");
        mEdtUUID.setText("6e455b0d-3be1-4052-8ba4-99b270220000");

        mBtnSave.setOnClickListener(saveListener);
    }

    private View.OnClickListener saveListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(canSave()) {
                storeInformation();
                Intent homeActivity = new Intent(SettingsActivity.this, GatewaysActivity.class);
                startActivity(homeActivity);
                finish();
            } else {
                Snackbar.make(mRootLayout,R.string.settings_invalid_fields, Snackbar.LENGTH_LONG).show();
            }
        }
    };

    private void storeInformation() {
        PreferenceUtil.getInstance().setEndPoint(mEdtEndPoint.getText().toString());
        PreferenceUtil.getInstance().setUuid(mEdtUUID.getText().toString());
        PreferenceUtil.getInstance().setToken(mEdtToken.getText().toString());
    }

    private boolean canSave() {
        return mEdtEndPoint != null &&  !mEdtEndPoint.getText().toString().equals(Util.EMPTY_STRING)  &&
                mEdtUUID != null && !mEdtUUID.getText().toString().equals(Util.EMPTY_STRING) &&
                mEdtToken != null && !mEdtToken.getText().toString().equals(Util.EMPTY_STRING);
    }
}
