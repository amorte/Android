package br.com.dts.knotintegration.service;

import java.util.List;

import br.com.dts.knotintegration.model.SwitchData;
import br.org.cesar.knot.lib.model.Data;

/**
 * Created by diegosouza on 1/29/18.
 */

public interface OnDataChangedListener {

    public void onDataChanged (List<SwitchData> deviceData);
}
