package br.com.dts.knotintegration.service;

import android.os.Binder;

/**
 * Created by diegosouza on 1/28/18.
 */

public class ServiceBinder extends Binder {
    private IKnotServiceConnection mBinder;

    public ServiceBinder(IKnotServiceConnection s) {
        mBinder = s;
    }

    public IKnotServiceConnection getService() {
        return mBinder;
    }

}
