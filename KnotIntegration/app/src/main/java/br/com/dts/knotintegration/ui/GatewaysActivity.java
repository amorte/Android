package br.com.dts.knotintegration.ui;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import br.com.dts.knotintegration.R;
import br.com.dts.knotintegration.model.SwitchDevice;
import br.com.dts.knotintegration.ui.adapter.SwitchDevicesAdapter;
import br.com.dts.knotintegration.util.Logger;
import br.com.dts.knotintegration.util.PreferenceUtil;
import br.com.dts.knotintegration.util.Util;
import br.org.cesar.knot.lib.connection.FacadeConnection;
import br.org.cesar.knot.lib.exception.InvalidDeviceOwnerStateException;
import br.org.cesar.knot.lib.exception.KnotException;
import br.org.cesar.knot.lib.model.KnotList;

public class GatewaysActivity extends AppCompatActivity {

    private ListView mListView;

    List<SwitchDevice> mDevicesList = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new SyncData().execute();
    }

    private void initListView(){
        mListView = new ListView(this);

        mListView.setAdapter(new SwitchDevicesAdapter(this, mDevicesList));

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SwitchDevice device = (SwitchDevice) parent.getItemAtPosition(position);

                if (device != null) {
                    //Toast.makeText(GatewaysActivity.this, device.toString(), Toast.LENGTH_LONG).show();
                    Intent it = new Intent(GatewaysActivity.this, DevicesActivity.class);
                    it.putExtra(Util.EXTRA_DEVICE_UUID, device.getUuid());
                    startActivity(it);
                }
            }
        });

        setContentView(mListView);

    }

    public class SyncData extends AsyncTask <Object, Object , List<SwitchDevice>>{
        @Override
        protected List<SwitchDevice> doInBackground(Object[] objects) {
            KnotList<SwitchDevice> list = new KnotList<>(SwitchDevice.class);
            try {

                FacadeConnection.getInstance().setupHttp(PreferenceUtil.getInstance().getEndPoint(), PreferenceUtil.getInstance().getUuid(),PreferenceUtil.getInstance().getTonken());

                mDevicesList = FacadeConnection.getInstance().httpGetDeviceList(list);

                Logger.d("Size = " + mDevicesList.size());
            } catch (KnotException e) {
                Logger.d("KnotExecption");
                e.getMessage();
                e.printStackTrace();
            } catch (InvalidDeviceOwnerStateException e) {
                Logger.d("InvalidDeviceOwnerStateException");
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(List<SwitchDevice> list) {
            super.onPostExecute(list);
            initListView();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(R.string.menu_settings);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case 0:
                startActivity(new Intent(this, SettingsActivity.class));
                break;
        }


        return super.onOptionsItemSelected(item);
    }
}
