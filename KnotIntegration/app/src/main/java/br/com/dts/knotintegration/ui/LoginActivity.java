package br.com.dts.knotintegration.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;

import android.view.View;
import android.view.View.OnClickListener;

import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import br.com.dts.knotintegration.R;
import br.com.dts.knotintegration.communication.KnotSocketIOCommunication;
import br.com.dts.knotintegration.util.PreferenceUtil;
import br.com.dts.knotintegration.util.Util;
import br.org.cesar.knot.lib.event.Event;


public class LoginActivity extends AppCompatActivity {


    private AutoCompleteTextView mUUIDView;
    private EditText mTokenView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mUUIDView = (AutoCompleteTextView) findViewById(R.id.email);

        mUUIDView.setText(Util.DEFAULT_UUID);

        mTokenView = (EditText) findViewById(R.id.password);

        mTokenView.setText(Util.DEFAULT_TOKEN);

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        //createSocketConnection();

    }


    private void attemptLogin() {

        PreferenceUtil.getInstance().setUuid(mUUIDView.getText().toString());
        PreferenceUtil.getInstance().setToken(mTokenView.getText().toString());
        PreferenceUtil.getInstance().setEndPoint(Util.KNOT_URL);

        startActivity(new Intent(this, GatewaysActivity.class));
        finish();

    }

    private void temp(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                KnotSocketIOCommunication.getInstance().getAllDevices();
            }
        }).start();

    }

    private void createSocketConnection(){
        KnotSocketIOCommunication.getInstance().authenticateSocketCommunication(new Event<Boolean>() {
            @Override
            public void onEventFinish(Boolean object) {
                temp();
            }

            @Override
            public void onEventError(Exception e) {
                e.printStackTrace();
            }
        });
    }

}

