
package br.com.dts.knotintegration.service;

import br.com.dts.knotintegration.model.SwitchData;
import br.com.dts.knotintegration.model.SwitchDevice;
import br.org.cesar.knot.lib.model.Data;
import br.org.cesar.knot.lib.model.KnotList;

/**
 * Created by diegosouza on 1/28/18.
 */

public interface IKnotServiceConnection {
    void subscribe(String deviceUUID, OnDataChangedListener listener);
    void unsubscribe();

}
