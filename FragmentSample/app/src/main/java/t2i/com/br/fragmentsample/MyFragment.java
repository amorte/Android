package t2i.com.br.fragmentsample;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wallace on 16/12/17.
 */

public class MyFragment extends Fragment {

    private static MyFragment myFragment;
    private ListView itensListView;

    //Metodo estatico para criar instancia do fragment
    public static MyFragment newInstance() {
        if(myFragment == null) {
            myFragment = new MyFragment();
        }

        return myFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_sample, container, false);

        //Associa o listView ao id da view no xml.
        itensListView = root.findViewById(R.id.lvTeams);

        List<Team> itens = new ArrayList<>();
        itens.add(new Team("Korea", R.drawable.desconhecido));
        itens.add(new Team("Argetina", R.drawable.argentina));
        itens.add(new Team("Portugal", R.drawable.portugal));
        itens.add(new Team("Chile", R.drawable.images));
        itens.add(new Team("Italia", R.drawable.italia));
        itens.add(new Team("Brasil", R.drawable.brasil));

        //Cria o adapter customizado que sera exibido no listView
        MyCustomAdapter adapter = new MyCustomAdapter(getActivity(), itens);

        //Seta o adapter ao listView
        itensListView.setAdapter(adapter);

        return root;
    }
}
